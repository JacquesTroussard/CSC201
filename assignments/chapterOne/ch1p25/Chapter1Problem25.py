# Northern Virginia Community College
# Jacques J Troussard
# Professor David Seaman
# CSC201
# 08/29/2015

# Purpose: This program will read user input in seconds (1-86,400)
# and return the total amount of hours, minutes and seconds in mil
# time format.

#Prompt user for input and convert String into int
user_input_str = input("Enter time in seconds (1-86,400): ")
user_input_int = int(user_input_str)

#Validate user input (is it within 1 and 86,400 seconds)
if(user_input_int < 1 or user_input_int > 86400):
    print("You entered an invalid number")
    exit()

#Convert user input into hours, then subtract hours from input.
#Convert remaining input into minutes, then subtract from
#remaining input. The remainder will be in seconds.
hours = user_input_int // 3600
remaining_input = user_input_int - (hours * 3600)
minutes = remaining_input // 60
seconds = remaining_input - (minutes * 60)

#Print results
print ("You entered, ", user_input_int)
print ("Hours ",hours,"Minutes ",minutes,"Seconds",seconds)
print (hours,":",minutes,":",seconds)