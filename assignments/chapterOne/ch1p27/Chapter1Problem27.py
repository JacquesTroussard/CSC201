# Northern Virginia Community College
# Jacques J Troussard
# Professor David Seaman
# CSC201
# 08/29/2015

#Purpose: This program will calculate the angles of a triangle given
# the users input for its three sides.

import math

#User input
print ("Enter the lengths of each side of the triangle...")
leg_a_str = input("First leg: ")
leg_b_str = input("Second leg: ")
leg_c_str = input("Third leg: ")

#Convert user input into variable type FLOAT
a_flt = float(leg_a_str)
b_flt = float(leg_b_str)
c_flt = float(leg_c_str)

#Verify user input
if(a_flt + b_flt <= c_flt or b_flt + c_flt <= a_flt):
    print("The length of sides you've entered do not form a triangle."\
          " Remember the \"Triangle inequality theorem\" which states that"\
          " the sum of the side lengths of any two sides of a triangle must "\
          "not exceed the length of the third side.")
    exit()
print()

#Print user input for test and user verification
print("Your triangle dimensions are: ")
print("leg a = ", a_flt, " leg b = ", b_flt, " leg c = ", c_flt)
print()

#Calculation angle (A)
cos_a = (a_flt ** 2 - (b_flt ** 2 + c_flt ** 2)) / (-2 * b_flt * c_flt)
angle_a = math.acos(cos_a)
print("Angle (A) is ", math.degrees(angle_a))

#Calculation angle (B)
cos_b = (b_flt ** 2 - (a_flt ** 2 + c_flt ** 2)) / (-2 * a_flt * c_flt)
angle_b = math.acos(cos_b)
print("Angle (B) is ", math.degrees(angle_b))

#Calculation angle (C)
cos_c = (c_flt ** 2 - (b_flt ** 2 + a_flt ** 2)) / (-2 * b_flt * a_flt)
angle_c = math.acos(cos_c)
print("Angle (C) is ", math.degrees(angle_c))
