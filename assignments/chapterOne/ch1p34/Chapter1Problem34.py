# Northern Virginia Community College
# Jacques J Troussard
# Professor David Seaman
# CSC201
# 09/03/2015

# Purpose: This program will calculate BMI in two standards. The
#  program will ask the user for data type; metric or imperial, then
#  prompt the user for weight and hieght mesaurements. Finally the
#  program will print the results.

import math
import numbers

#Declaration of constants
METER_2_FEET = 3.28084
KILOGRAMS_2_POUNDS = 2.20462

    #Introduction and standard selection
    print("###### PYTHON BMI CALCULATOR ######")
    print()
    print("Will you be calculating BMI by meteric or imperial standards?")
    print("(1) Metric")
    print("(2) Imperial")
    standard_check = input(">>>")

    #Validate standard selection (metric)
    if (standard_check == "1"):
        print("You selected metric.")

        #Read user input METRIC
        m_weight_str = input("Enter weight in kilograms: ")
        m_weight_flt = float(m_weight_str)
        print("You entered, ", m_weight_flt, "kg.")
        m_height_str = input("Enter height in meters: ")
        m_height_flt = float(m_height_str)
        print("You entered, ", m_height_flt, "m.")
        
        #Calculate BMI METRIC input
        bmi = m_weight_flt / (math.pow(m_height_flt, 2))
        print()
        print("The calculated BMI is ", math.trunc(bmi))
        
        #Validate standard selection (imperial)
        
    elif (standard_check == "2"):
        print("You selected imperial.")
        #Read user input IMPERIAL
        imp_weight_str = input("Enter weight in pounds: ")
        imp_weight_flt = float(imp_weight_str)
        imp_weight_converted = imp_weight_flt / KILOGRAMS_2_POUNDS
        print("You entered ", imp_weight_str,"lbs. which converts to %.2f" \                                                                                % imp_weight_converted, "kg.")
        
        
        #Check user input for decmial style or feet' + inches" style
        #in_height_str = input("Enter height in feet: ")
        
        imp_height_flt = float(imp_height_str)
        if float.is_integer(imp_height_flt):
            #Calculate feet' inches" style into meters
            imp_feet_flt = imp_height_flt
            imp_inches_str = input("Enter additional inches if necessary "
                    "(enter zero for none): ")
            
            imp_inches_flt = float(imp_inches_str)
            imp_height_flt = imp_feet_flt + (imp_inches_flt / 12)
            imp_height_converted = imp_height_flt / METER_2_FEET
            print("You entered %s' %s\" which converts into %.2fm."
                    % (int(imp_feet_flt), int(imp_inches_flt),
                        imp_height_converted))
                                                                                                                                
                    #Calculate decimal feet style into meters
                    
        else:
            imp_height_converted = imp_height_flt / METER_2_FEET
            print('You entered ' + imp_height_str + '"' + ' which converts '
                    'into %.2fm,' % (imp_height_converted))
    else:
        print("Invalid data.")
        exit()
        
        #Calculate BMI IMPERIAL input
        bmi = imp_weight_converted / (math.pow(imp_height_converted, 2))
        if bmi < 18.5:
            print("The calculated BMI is %.2f which is classified as underweight."
                    % (bmi))
        elif 18.5 <= bmi <= 24.9:
            print("The calculated BMI is %.2f which is classified as normal."
                    % (bmi))
        elif 25 <= bmi < 29.9:
            print("The calculated BMI is %.2f which is classified as overweight."
                    % (bmi))
        else:
            print("The calculated BMI is %.2f which is classified as obese."
                    % (bmi))
