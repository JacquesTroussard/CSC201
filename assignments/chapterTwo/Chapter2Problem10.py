#  Purpose: This program will determine if a user input is a perfect square.

import math

#Boolean initialization
loop_ctrl = True
perfect_square = True

print ("#### Welcome to PYTHON Perfect Square Calculator ####")
while loop_ctrl:
    if perfect_square == False:
        print("%s is not a perfect square. Try again." % user_input)
        print()
        user_input_str = input("Enter a number: ")
        user_input = int(user_input_str)
        for counter in range(1, user_input):
            square_check = math.pow(counter, 2)
            if square_check == user_input:
                perfect_square = True
                loop_ctrl = False
                print("%s is a perfect square!" % user_input)
            else:
                perfect_square = False
